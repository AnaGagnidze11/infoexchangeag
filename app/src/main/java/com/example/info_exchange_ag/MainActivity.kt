package com.example.info_exchange_ag

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val colorRequestCode = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        secondPageButton.setOnClickListener {
            openSecondPage()
        }
    }
    private fun openSecondPage(){
        val name = nameEditText.text.toString()
        val lastName = lastNameEditText.text.toString()
        val age = ageEditText.text.toString().toInt()

        val intent = Intent(this, SecondPageActivity::class.java)
        intent.putExtra("name", name)
        intent.putExtra("last_name", lastName)
        intent.putExtra("age", age)
        startActivityForResult(intent, colorRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == colorRequestCode && resultCode == Activity.RESULT_OK){
            val color = data?.extras?.getString("color", "")
            colorTextView.text = color
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}