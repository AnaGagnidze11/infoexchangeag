package com.example.info_exchange_ag

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second_page.*

class SecondPageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_page)
        init()
    }
    private fun init(){
       val firstName =  intent.extras?.getString("name", "")
        val lastName =  intent.extras?.getString("last_name", "")
        val age = intent.extras?.getInt("age", 0)

        nameTextView.text = firstName
        lastNameTextView.text = lastName
        ageTextView.text = age.toString()

        returnColorButton.setOnClickListener {
            returnColor()
        }
    }

    private fun returnColor(){
        val color = colorEditText.text.toString()
        val intent = intent
        intent.putExtra("color", color)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}